package com.example.quickart.ui.login;

import android.Manifest;
import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.quickart.R;
import com.example.quickart.ScanActivity;
import com.example.quickart.Utils;
import com.example.quickart.WelcomeActivity;
import com.example.quickart.data.ItemDetail;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;
    private FirebaseAuth mAuth;
    private String phoneNumber;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    private String verificationId;
    private ProgressBar loadingProgressBar;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(LoginActivity.this, new
                    String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        }

        setContentView(R.layout.activity_login);
        loginViewModel = new ViewModelProvider(this, new LoginViewModelFactory())
                .get(LoginViewModel.class);


        SharedPreferences shared = getSharedPreferences("shared",MODE_PRIVATE);
        if(shared.contains("phoneNumber")){
            //Redirect to WelcomeActivity
            Intent intent = new Intent(LoginActivity.this, WelcomeActivity.class);
            startActivity(intent);
            return;
        }

        final EditText mobileNumberText = findViewById(R.id.mobileNumber);
        final Button getOTPBtn = findViewById(R.id.getOTP);
        loadingProgressBar = findViewById(R.id.loading);
        loadingProgressBar.setVisibility(View.INVISIBLE);
        final View view = this.getCurrentFocus();
        final Activity activity = this;
        mAuth = FirebaseAuth.getInstance();
        final Button verifyOTPBtn = findViewById(R.id.verifyOTPBtn);
        final EditText verifyOTPTxt = findViewById(R.id.verifyOTPTxt);

        verifyOTPBtn.setEnabled(false);
        verifyOTPTxt.setEnabled(false);
        verifyOTPTxt.setVisibility(View.INVISIBLE);

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                getOTPBtn.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    mobileNumberText.setError(getString(loginFormState.getUsernameError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResult.getError() != null) {
                    showLoginFailed(loginResult.getError());
                }
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                }
                setResult(Activity.RESULT_OK);

                //Complete and destroy login activity once successful
                finish();
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(mobileNumberText.length() == 10){
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(findViewById(R.id.mobileNumber).getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    getOTPBtn.setEnabled(true);
                }else{
                    getOTPBtn.setEnabled(false);
                }
            }

        };
        mobileNumberText.addTextChangedListener(afterTextChangedListener);

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                Log.i("Verify complete ","In verify compleletttt   "+phoneNumber);

                final String code = phoneAuthCredential.getSmsCode();
                if(code != null){
                    verifyOTPTxt.setText(code);
                    verifyOTP(code);
                }
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {

            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationId = s;
            }
        };

        verifyOTPBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(verifyOTPTxt.getText().toString())){
                    Toast.makeText(LoginActivity.this,"Please enter OTP",Toast.LENGTH_SHORT).show();
                }else{
                    verifyOTP(verifyOTPTxt.getText().toString());
                }
            }
        });

        getOTPBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    phoneNumber = mobileNumberText.getText().toString();

                    loadingProgressBar.setVisibility(View.VISIBLE);
                    PhoneAuthOptions options = PhoneAuthOptions.newBuilder(mAuth).setPhoneNumber("+91"+phoneNumber).setActivity(activity).setTimeout(60L, TimeUnit.SECONDS)
                            .setCallbacks(mCallbacks).build();

                    PhoneAuthProvider.verifyPhoneNumber(options);

                    verifyOTPBtn.setEnabled(true);
                    verifyOTPTxt.setEnabled(true);
                    verifyOTPTxt.setVisibility(View.VISIBLE);

                }
                catch (Exception e){
                    e.printStackTrace();
                }

//                loginViewModel.login(mobileNumberText.getText().toString(), "");
            }
        });
    }

    private void verifyOTP(String otp){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId,otp);
        signInWithCredential(credential);
    }

    private void signInWithCredential(PhoneAuthCredential credential){
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){


                    SharedPreferences shared = getSharedPreferences("shared",MODE_PRIVATE);
                    SharedPreferences.Editor editor = shared.edit();

                    editor.putString("phoneNumber", phoneNumber);
                    editor.commit();


                    loadingProgressBar.setVisibility(View.INVISIBLE);

                    //Check if the phoneNumber is registered. Else, Register user.

//                    new HttpAsyncTask().execute();
                    Intent intent = new Intent(LoginActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(LoginActivity.this,task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + model.getDisplayName();
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    private class HttpAsyncTask extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {
            try {

                KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
                trustStore.load(null, null);

                MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
                sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

                HttpParams params = new BasicHttpParams();
                HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
                HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

                SchemeRegistry registry = new SchemeRegistry();
                registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
                registry.register(new Scheme("https", sf, 443));

                ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);


                //instantiates httpclient to make request
                DefaultHttpClient httpclient = new DefaultHttpClient(ccm,params);
                String baseURL = Utils.getString("serverBaseURL",LoginActivity.this.getBaseContext());
                //url with the post data
                HttpPost httpost = new HttpPost(baseURL + "checkMobileExists");

                Map<String, Map<String,String>> requestMap = new HashMap<>();
                Map<String,String> phoneNumberMap = new HashMap<>();
                phoneNumberMap.put("phoneNumber", phoneNumber);

                requestMap.put("data", phoneNumberMap);

                //convert parameters into JSON object
                JSONObject holder = new JSONObject(requestMap);

                //passes the results to a string builder/entity
                StringEntity se = new StringEntity(holder.toString());

                //sets the post request as the resulting string
                httpost.setEntity(se);
                //sets a request header so the page receving the request
                //will know what to do with it
                httpost.setHeader("Accept", "application/json");
                httpost.setHeader("Content-type", "application/json");

                //Handles what is returned from the page
                HttpResponse response =  httpclient.execute(httpost);
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    String responseString = EntityUtils.toString(response.getEntity());

                    JSONObject jsonObject = new JSONObject(responseString);
                    if(jsonObject.getJSONObject("data").getInt("statusCode") != HttpStatus.SC_OK){
                        Intent intent = new Intent(LoginActivity.this,RegistrationActivity.class);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(LoginActivity.this,WelcomeActivity.class);
                        startActivity(intent);
                    }

                    return responseString;


                }


//                HttpClient httpClient = new DefaultHttpClient();
//                HttpResponse response = httpClient.execute(new HttpPost("http://192.168.0.199:8080/getInventoryItem?id=100"));
//                StatusLine statusLine = response.getStatusLine();
//                if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
//                    String responseString = EntityUtils.toString(response.getEntity());
//                    return responseString;
//                }
            }
            catch(Exception e){
                e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {


            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    public class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[] { tm }, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }

}