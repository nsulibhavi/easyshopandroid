package com.example.quickart.ui.login;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.quickart.ScanActivity;
import com.example.quickart.Utils;
import com.example.quickart.WelcomeActivity;
import com.example.quickart.data.model.RegisterUserModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.quickart.R;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {

    private EditText userName;
    private EditText emailId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Button signupBtn = findViewById(R.id.signupBtn);
        userName = findViewById(R.id.userName);
        emailId = findViewById(R.id.editEmail);
        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                new HttpAsyncTask().execute();
                Intent welcomeActivity = new Intent(RegistrationActivity.this, WelcomeActivity.class);
                startActivity(welcomeActivity);
                //Send Http Post to register API
                try {
//                    RegisterUserModel regUser = new RegisterUserModel();
//                    HttpClient httpClient = new DefaultHttpClient();
//                    HttpResponse response = httpClient.execute(new HttpPost(""));
//                    StatusLine statusLine = response.getStatusLine();
//                    if(statusLine.getStatusCode() == HttpStatus.SC_OK){
//
//                    }


                }
                catch(Exception e){

                }
            }
        });
    }

    private class HttpAsyncTask extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {
            try {

                //instantiates httpclient to make request
                DefaultHttpClient httpclient = new DefaultHttpClient();
                String baseURL = Utils.getString("serverBaseURL",RegistrationActivity.this.getBaseContext());
                //url with the post data
                HttpPost httpost = new HttpPost(baseURL + "/register");

                Map<String, Map<String,String>> requestMap = new HashMap<>();
                Map<String,String> phoneNumberMap = new HashMap<>();
                phoneNumberMap.put("userName", userName.getText().toString());
                phoneNumberMap.put("emailId", emailId.getText().toString());

                requestMap.put("data", phoneNumberMap);

                //convert parameters into JSON object
                JSONObject holder = new JSONObject(requestMap);

                //passes the results to a string builder/entity
                StringEntity se = new StringEntity(holder.toString());

                //sets the post request as the resulting string
                httpost.setEntity(se);
                //sets a request header so the page receving the request
                //will know what to do with it
                httpost.setHeader("Accept", "application/json");
                httpost.setHeader("Content-type", "application/json");

                //Handles what is returned from the page
                HttpResponse response =  httpclient.execute(httpost);
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    String responseString = EntityUtils.toString(response.getEntity());
                    Intent welcomeActivity = new Intent(RegistrationActivity.this, WelcomeActivity.class);
                    startActivity(welcomeActivity);

                    return responseString;
                }


//                HttpClient httpClient = new DefaultHttpClient();
//                HttpResponse response = httpClient.execute(new HttpPost("http://192.168.0.199:8080/getInventoryItem?id=100"));
//                StatusLine statusLine = response.getStatusLine();
//                if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
//                    String responseString = EntityUtils.toString(response.getEntity());
//                    return responseString;
//                }
            }
            catch(Exception e){
                e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {


            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    }
}