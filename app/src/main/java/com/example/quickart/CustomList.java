package com.example.quickart;

import android.app.Activity;
import android.os.Bundle;

import com.example.quickart.data.ItemDetail;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class CustomList extends ArrayAdapter {

    private final Activity context;
    private final List<ItemDetail> items;

    public CustomList(Activity context, List<ItemDetail> items){
        super(context,R.layout.activity_cutom_list,items);

        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
//        return super.getCount();
        return items.size();
    }

    public void updateList(ItemDetail item){
        this.items.add(item);
//        this.notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        Log.i("Listttt","Insideeeee Listtatatt");
        View rowView = inflater.inflate(R.layout.activity_cutom_list,null, true);

        TextView itemName = (TextView) rowView.findViewById(R.id.itemName);
        ImageButton plusBtn = (ImageButton) rowView.findViewById(R.id.itemAddBtn);
        ImageButton minusBtn = (ImageButton) rowView.findViewById(R.id.itemMinusBtn);
        TextView itemQty = (TextView) rowView.findViewById(R.id.itemQty);
        TextView itemPrice = (TextView) rowView.findViewById(R.id.itemPrice);
//        TextView itemTotal = (TextView) rowView.findViewById(R.id.itemTotalPrice);
//        Button deleteBtn = (Button) rowView.findViewById(R.id.deleteBtn);

        itemName.setText(this.items.get(position).getDescription());
        itemQty.setText(" x 1");
        itemPrice.setText(this.items.get(position).getPrice());
        itemPrice.setText(this.items.get(position).getPrice());
//        itemName.setText("TEsttt");
//        itemQty.setText(" x 1");
//        itemPrice.setText("32");
//        itemTotal.setText("32");

        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = itemQty.getText().toString().split("x")[1].trim();
                Log.i("qty","dfsa     "+qty);
                int currentQty = Integer.parseInt(qty);
                currentQty += 1;
                itemQty.setText(" x "+ String.valueOf(currentQty));
//                itemTotal.setText(String.valueOf(currentQty * Integer.parseInt(itemPrice.getText().toString())));
            }
        });

        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentQty = Integer.parseInt(itemQty.getText().toString().split("x")[1].trim());
                currentQty -= 1;
                itemQty.setText(" x "+ String.valueOf(currentQty));
//                itemTotal.setText(String.valueOf(currentQty * Integer.parseInt(itemPrice.getText().toString())));
            }
        });

        return rowView;
    }

}