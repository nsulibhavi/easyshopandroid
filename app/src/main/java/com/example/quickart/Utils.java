package com.example.quickart;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Utils {

    public static String getString(String key, Context context) throws IOException {
        Properties prop = new Properties();
        AssetManager asstManager = context.getAssets();
        InputStream inputStream = asstManager.open("application.properties");
        prop.load(inputStream);
        return prop.getProperty(key);
    }

}
