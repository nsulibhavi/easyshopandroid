package com.example.quickart;

import android.content.Intent;
import android.os.Bundle;

import com.example.quickart.data.ItemDetail;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MyCart extends AppCompatActivity {
    CustomList adapter;
    ListView listView;

    //Footer buttons
    Button myAccountBtn;
    Button scanBtn;
    Button receiptsBtn;
    Button cartBtn;
    Button payBtn;
    String uniqueId;
    final List<ItemDetail> ListElementsArrayList = new ArrayList<ItemDetail>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listView = (ListView) findViewById(R.id.myCartList);
        uniqueId = (String)getIntent().getSerializableExtra("uniqueId");
        adapter = new CustomList(MyCart.this, ListElementsArrayList);
        listView.setAdapter(adapter);

        myAccountBtn = (Button)findViewById(R.id.myAccountBtn);
        scanBtn = (Button)findViewById(R.id.scanBtn);
        receiptsBtn = (Button)findViewById(R.id.receiptsBtn);
        cartBtn = (Button)findViewById(R.id.cartBtn);

        payBtn = (Button)findViewById(R.id.payBtn);

        //Set Listeners to footer buttons
        myAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyCart.this,MyAccount.class);
                startActivity(i);
            }
        });

        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyCart.this,ScanActivity.class);
                startActivity(i);
            }
        });

        receiptsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyCart.this,MyAccount.class);
                startActivity(i);
            }
        });

        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyCart.this,MyCart.class);
                startActivity(i);
            }
        });

        payBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyCart.this, Checkout.class);
                i.putExtra("uniqueId",uniqueId);
                startActivity(i);
            }
        });
    }
}