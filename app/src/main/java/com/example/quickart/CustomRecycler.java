package com.example.quickart;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quickart.data.ItemDetail;

import org.json.JSONStringer;

import java.util.List;

public class CustomRecycler extends RecyclerView.Adapter<CustomRecycler.ViewHolder> {
    private final Activity context;
    private final List<ItemDetail> items;
    private static TextView totalTxt;
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView itemName;
        private final Button plusBtn;
        private final Button minusBtn;
        private final TextView itemQty;
        private final TextView itemPrice;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            itemName = (TextView) view.findViewById(R.id.itemName);
            plusBtn = (Button) view.findViewById(R.id.plusBtn);
            minusBtn = (Button) view.findViewById(R.id.minusBtn);
            itemQty = (TextView) view.findViewById(R.id.itemQty);
            itemPrice = (TextView) view.findViewById(R.id.itemPrice);
            totalTxt = (TextView) view.findViewById(R.id.totalTxt);
        }

        public TextView getItemName() {
            return itemName;
        }
        public Button getPlusBtn() { return plusBtn; }

        public Button getMinusBtn() {
            return minusBtn;
        }

        public TextView getItemQty() {
            return itemQty;
        }

        public TextView getItemPrice() {
            return itemPrice;
        }

        public TextView getTotalTxt() {
            return totalTxt;
        }
    }

    public CustomRecycler(Activity context, List<ItemDetail> items) {
        this.context = context;
        this.items = items;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_cutom_list, parent, false);

        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element

        ItemDetail item = items.get(position);
        Log.i("re", item.getPrice());
        Log.i("re11", item.getDescription());
        Log.i("size", String.valueOf(items.size()));
        viewHolder.getItemName().setText(item.getDescription());
        viewHolder.getItemPrice().setText("INR " + item.getPrice());
        viewHolder.getItemQty().setText("1");


        viewHolder.getPlusBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String qty = viewHolder.getItemQty().getText().toString();
                Log.i("qty","dfsa     "+qty);
                int currentQty = Integer.parseInt(qty);
                currentQty += 1;
                viewHolder.getItemQty().setText(String.valueOf(currentQty));
            }
        });

        viewHolder.getMinusBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int currentQty = Integer.parseInt(viewHolder.getItemQty().getText().toString());
                currentQty -= 1;
                if(currentQty == 0) {
                    //Display alert for low quantity
                    AlertDialog alertDialog = new AlertDialog.Builder(CustomRecycler.this.context).create();
                    alertDialog.setTitle("Confirm");
                    alertDialog.setMessage("Are you sure you want to remove?");

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            items.remove(viewHolder);
                        }
                    });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                }
                viewHolder.getItemQty().setText(String.valueOf(currentQty));
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.items.size();
    }

}
