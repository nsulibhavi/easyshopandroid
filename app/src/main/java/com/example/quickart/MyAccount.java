package com.example.quickart;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.quickart.ui.login.RegistrationActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class MyAccount extends AppCompatActivity {

    ImageView userImg;
    TextView fullNameTxt;
    TextView emailTxt;
    Button myCartBtn;
    Button myReceiptsBtn;
    Button logoutBtn;

    //Footer buttons
    Button myAccountBtn;
    Button scanBtn;
    Button receiptsBtn;
    Button cartBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        userImg = (ImageView)findViewById(R.id.userImg);
        fullNameTxt = (TextView)findViewById(R.id.fullNameTxt);
        emailTxt = (TextView)findViewById(R.id.emailTxt);
        myCartBtn = (Button)findViewById(R.id.myCartBtn);
        myReceiptsBtn = (Button)findViewById(R.id.myReceiptsBtn);
        logoutBtn = (Button)findViewById(R.id.logoutBtn);

        //Footer Buttons
        myAccountBtn = (Button)findViewById(R.id.myAccountBtn);
        scanBtn = (Button)findViewById(R.id.scanBtn);
        receiptsBtn = (Button)findViewById(R.id.receiptsBtn);
        cartBtn = (Button)findViewById(R.id.cartBtn);


        //Make a http call to getAccountDetails.
        new HttpAsyncFetchAccountDetails().execute();

        //Set Listeners to footer buttons
        myAccountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyAccount.this,MyAccount.class);
                startActivity(i);
            }
        });

        scanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyAccount.this,ScanActivity.class);
                startActivity(i);
            }
        });

        receiptsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyAccount.this,MyAccount.class);
                startActivity(i);
            }
        });

        cartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyAccount.this,MyCart.class);
                startActivity(i);
            }
        });

        //Set click listeners for account buttons
        myCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyAccount.this,MyCart.class);
                startActivity(i);
            }
        });

        myReceiptsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyAccount.this,MyAccount.class);
                startActivity(i);
            }
        });

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //To do logout
            }
        });
    }

    private class HttpAsyncFetchAccountDetails extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {
            try {

                //instantiates httpclient to make request
                DefaultHttpClient httpclient = new DefaultHttpClient();
                String baseURL = Utils.getString("serverBaseURL", MyAccount.this.getBaseContext());
                //url with the post data
                HttpPost httpost = new HttpPost(baseURL + "/getAccountDetails");
                SharedPreferences shared = getSharedPreferences("shared",MODE_PRIVATE);
                String phoneNumber = shared.getString("phoneNumber","");
                Map<String, Map<String,String>> requestMap = new HashMap<>();
                Map<String,String> phoneNumberMap = new HashMap<>();
                phoneNumberMap.put("phoneNumber", phoneNumber);
                requestMap.put("data", phoneNumberMap);

                //convert parameters into JSON object
                JSONObject holder = new JSONObject(requestMap);

                //passes the results to a string builder/entity
                StringEntity se = new StringEntity(holder.toString());

                //sets the post request as the resulting string
                httpost.setEntity(se);
                //sets a request header so the page receving the request
                //will know what to do with it
                httpost.setHeader("Accept", "application/json");
                httpost.setHeader("Content-type", "application/json");

                //Handles what is returned from the page
                HttpResponse response =  httpclient.execute(httpost);
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    String responseString = EntityUtils.toString(response.getEntity());
                    Intent welcomeActivity = new Intent(MyAccount.this, WelcomeActivity.class);
                    JSONObject jsonObject = new JSONObject(responseString);
                    fullNameTxt.setText(jsonObject.getJSONObject("data").getString("firstName") +" "+jsonObject.getJSONObject("data").getString("lastName"));
                    emailTxt.setText(jsonObject.getJSONObject("data").getString("email"));
                    startActivity(welcomeActivity);

                    return responseString;
                }


//                HttpClient httpClient = new DefaultHttpClient();
//                HttpResponse response = httpClient.execute(new HttpPost("http://192.168.0.199:8080/getInventoryItem?id=100"));
//                StatusLine statusLine = response.getStatusLine();
//                if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
//                    String responseString = EntityUtils.toString(response.getEntity());
//                    return responseString;
//                }
            }
            catch(Exception e){
                e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {


            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    }
}