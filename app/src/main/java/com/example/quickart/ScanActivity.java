package com.example.quickart;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.quickart.data.ItemDetail;
import com.example.quickart.data.model.RegisterUserModel;
import com.example.quickart.ui.login.RegistrationActivity;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.ncorti.slidetoact.SlideToActView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ScanActivity extends AppCompatActivity {

    SurfaceView surfaceView;
    ListView listView;
    private BarcodeDetector barcodeDetector;
    Button checkoutBtn;
    private CameraSource cameraSource;
    private static final int REQUEST_CAMERA_PERMISSION = 201;
    final List<ItemDetail> ListElementsArrayList = new ArrayList<ItemDetail>();
    final List<String> scannedBarcodes = new ArrayList<String>();
    CustomList adapter;
//    CustomListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listView = (ListView) findViewById(R.id.listView);
        surfaceView = findViewById(R.id.surfaceView);

        adapter = new CustomList(ScanActivity.this, ListElementsArrayList);
        listView.setAdapter(adapter);
        checkoutBtn = (Button)findViewById(R.id.checkoutBtn);

        checkoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Send a post request to backend to save cart

                Intent i = new Intent(ScanActivity.this, MyCart.class);
                String uniqueId = "";
                SharedPreferences shared = getSharedPreferences("shared",MODE_PRIVATE);
                String phoneNumber = shared.getString("phoneNumber","");

                long currentTime = new Date().getTime();
                uniqueId = phoneNumber+"_"+String.valueOf(currentTime);
                Gson gson = new Gson();
                String jsonObject = gson.toJson(ListElementsArrayList);
                i.putExtra("uniqueId",uniqueId);
                i.putExtra("cartItemsList",jsonObject);
                startActivity(i);
            }
        });

    }

    private void initializeDetectors(){
        barcodeDetector = new BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.ALL_FORMATS).build();

        cameraSource = new CameraSource.Builder(this,barcodeDetector).setRequestedPreviewSize(1920,1080).setAutoFocusEnabled(true).build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(@NonNull SurfaceHolder holder) {
                try {
                    if(ActivityCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                        cameraSource.start(surfaceView.getHolder());
                    }else{
                        ActivityCompat.requestPermissions(ScanActivity.this, new
                                String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
                cameraSource.stop();
            }
        });
        Tracker<Barcode> myTracker = new Tracker<>();
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
                if(ActivityCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
                    try {
                        cameraSource.start(surfaceView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    ActivityCompat.requestPermissions(ScanActivity.this, new
                            String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
                    try {
                        cameraSource.start(surfaceView.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
//                Toast.makeText(getApplicationContext(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {


                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if(barcodes.size() != 0){
                    if(barcodes.valueAt(0) != null) {
                        String readBarcodeValue = barcodes.valueAt(0).rawValue;
                        if(!scannedBarcodes.contains(readBarcodeValue)){
                            scannedBarcodes.add(readBarcodeValue);
//                            new HttpAsyncTask().execute();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

//                                    new HttpAsyncTask().execute();
                                    String[] itemName = {"Choco Stick","Fruitella","Dairy Milk Silk","Calculator"};
                                    Random rand = new Random();

                                    ItemDetail item = new ItemDetail();

                                    item.setDescription(itemName[rand.nextInt(itemName.length)]);
                                    item.setImageURL("");
                                    item.setAvailableQuantity("20");
                                    item.setPrice("50");
                                    ListElementsArrayList.add(item);
                                    adapter.notifyDataSetChanged();
//                                    adapter.updateList(item);
//                                    adapter.notifyItemInserted(ListElementsArrayList.size() - 1);
                                }
                            });

                        }
//                        Thread requestThread = new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                try {
//                                    if(!scannedBarcodes.contains(readBarcodeValue)){
//                                        scannedBarcodes.add(readBarcodeValue);
//                                        HttpClient httpClient = new DefaultHttpClient();
//                                        HttpResponse response = httpClient.execute(new HttpGet("http://192.168.0.199:8080/getInventoryItem?id=100"));
//                                        StatusLine statusLine = response.getStatusLine();
//                                        if(statusLine.getStatusCode() == HttpStatus.SC_OK){
//                                            String responseString = EntityUtils.toString(response.getEntity());
//                                            runOnUiThread(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    try {
//
//                                                        JSONObject jsonObject = new JSONObject(responseString);
//                                                        ItemDetail item = new ItemDetail();
//                                                        item.setDescription(jsonObject.getJSONObject("data").getString("description"));
//                                                        item.setImageURL(jsonObject.getJSONObject("data").getString("imageURL"));
//                                                        item.setAvailableQuantity(jsonObject.getJSONObject("data").getInt("availableQuantity"));
//                                                        item.setPrice(jsonObject.getJSONObject("data").getInt("price"));
//                                                        item.setPrice(jsonObject.getJSONObject("data").getInt("itemId"));
//
//                                                        ListElementsArrayList.add(item);
//                                                        adapter.notifyDataSetChanged();
//                                                    }
//                                                    catch(Exception e){
//                                                        e.printStackTrace();
//                                                    }
//                                                }
//                                            });
//                                        }
//                                    }
//
//                                }catch(Exception e){
//                                    e.printStackTrace();
//                                }
//                            }
//                        });
//
//                        requestThread.start();
                    }else {
                        Toast.makeText(getApplicationContext(), "Unable to scan!!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraSource.release();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeDetectors();
    }

    private class HttpAsyncTask extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {
            try {
                HttpClient httpClient = new DefaultHttpClient();
                String baseURL = Utils.getString("serverBaseURL", ScanActivity.this.getBaseContext());
                //url with the post data
                HttpResponse response = httpClient.execute(new HttpGet(baseURL + "/getInventoryItem?id=100"));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
                    String responseString = EntityUtils.toString(response.getEntity());
                    JSONObject jsonObject = new JSONObject(responseString);

                    ItemDetail item = new ItemDetail();
                    item.setDescription(jsonObject.getJSONObject("data").getString("itemName"));
                    item.setImageURL("");
                    item.setAvailableQuantity(String.valueOf(jsonObject.getJSONObject("data").getInt("quantity")));
                    item.setPrice("50");
                    ListElementsArrayList.add(item);
                    adapter.notifyDataSetChanged();
                    return responseString;
                }
            }
            catch(Exception e){
                e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.i("adsad","safdsadasdddasdasdsa");
                JSONObject jsonObject = new JSONObject(s);
                ItemDetail item = new ItemDetail();
                item.setDescription(jsonObject.getJSONObject("data").getString("description"));
                item.setImageURL(jsonObject.getJSONObject("data").getString("imageURL"));
                item.setAvailableQuantity(String.valueOf(jsonObject.getJSONObject("data").getInt("availableQuantity")));
                item.setPrice(String.valueOf(jsonObject.getJSONObject("data").getInt("price")));
                item.setPrice(String.valueOf(jsonObject.getJSONObject("data").getInt("itemId")));
                Log.i("sdsadasda","Iteeeeemmmmmmmmmmmmmmmmmmmmmmm");

//                ListElementsArrayList.add(item);
//                adapter.updateList(item);
                Log.i("count","==   "+adapter.getCount());
                adapter.notifyDataSetChanged();
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    private class HttpSaveCartDetails extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... strings) {
            try {
                HttpClient httpClient = new DefaultHttpClient();
                String baseURL = Utils.getString("serverBaseURL", ScanActivity.this.getBaseContext());
                //url with the post data
                HttpResponse response = httpClient.execute(new HttpPost(baseURL + "/saveCartData"));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK) {
                    String responseString = EntityUtils.toString(response.getEntity());
                    JSONObject jsonObject = new JSONObject(responseString);

                    ItemDetail item = new ItemDetail();
                    item.setDescription(jsonObject.getJSONObject("data").getString("itemName"));
                    item.setImageURL("");
                    item.setAvailableQuantity(String.valueOf(jsonObject.getJSONObject("data").getInt("quantity")));
                    item.setPrice("50");
                    ListElementsArrayList.add(item);
                    adapter.notifyDataSetChanged();
                    return responseString;
                }
            }
            catch(Exception e){
                e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                Log.i("adsad","safdsadasdddasdasdsa");
                JSONObject jsonObject = new JSONObject(s);
                ItemDetail item = new ItemDetail();
                item.setDescription(jsonObject.getJSONObject("data").getString("description"));
                item.setImageURL(jsonObject.getJSONObject("data").getString("imageURL"));
                item.setAvailableQuantity(String.valueOf(jsonObject.getJSONObject("data").getInt("availableQuantity")));
                item.setPrice(String.valueOf(jsonObject.getJSONObject("data").getInt("price")));
                item.setPrice(String.valueOf(jsonObject.getJSONObject("data").getInt("itemId")));
                Log.i("sdsadasda","Iteeeeemmmmmmmmmmmmmmmmmmmmmmm");

//                ListElementsArrayList.add(item);
//                adapter.updateList(item);
                Log.i("count","==   "+adapter.getCount());
                adapter.notifyDataSetChanged();
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    }

}

